import { PerspectiveCamera, Scene, BoxGeometry, MeshNormalMaterial, Mesh, WebGLRenderer, RawShaderMaterial, TorusKnotGeometry } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import vertShader from "./glsl/torus.vs";
import fragShader from "./glsl/torus.fs";

export default class Renderer {
    private camera: THREE.PerspectiveCamera = new PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10);
    private scene: THREE.Scene = new Scene();
    private renderer: THREE.WebGLRenderer = new WebGLRenderer({antialias: true});
    private controls: OrbitControls = new OrbitControls(this.camera, this.renderer.domElement);

    private timeU: THREE.IUniform;

    constructor() {
        this.camera.position.z = 1;

        // let geometry = new BoxGeometry( 0.2, 0.2, 0.2);
        let geometry = new TorusKnotGeometry( 0.2, 0.2, 50, 20);
        let material = new RawShaderMaterial({
            uniforms: {
                time: {value: 0}
            },
            vertexShader: vertShader,
            fragmentShader: fragShader
        });
        this.timeU = material.uniforms.time;
        // let material = new MeshNormalMaterial();

        let mesh = new Mesh(geometry, material);

        this.scene.add(mesh);

        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setAnimationLoop(this.render);

        document.body.appendChild(this.renderer.domElement);
        window.addEventListener("resize", this.resize);
    }

    resize = () => {
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
    }

    render = () => {
        this.timeU.value += 0.02;
        this.controls.update();
        this.renderer.render(this.scene, this.camera);
    }
}