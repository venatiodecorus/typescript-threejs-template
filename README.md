# Note

This project is basically a fork of https://github.com/pinqy520/three-typescript-starter but includes some tweaks:

- Includes @types/three (original project relied on Three.js r125 which had type annotations included)
- Adds GLSL support from https://github.com/marquizzo/three.ts-template

Endless thanks to the original authors.

# Typescript Three.js Webpack Starter

- Typescript support.
- Webpack
- Html plugin

## Usage

### Install Typscript

```
npm install typescript -g
```

### Start

```
$ git clone https://gitlab.com/venatiodecorus/typescript-threejs-template.git
$ cd typescript-threejs-template
$ npm install # or yarn
$ npm start
```